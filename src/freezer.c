/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

#include <stdio.h>
#include <stdarg.h>

#include <gio/gio.h>

#include "freezer.h"

#define FREEZE_TIMEOUT 2000

#define LOGIND_DBUS_NAME            "org.freedesktop.login1"
#define LOGIND_DBUS_PATH            "/org/freedesktop/login1/seat/seat0"
#define LOGIND_DBUS_INTERFACE       "org.freedesktop.login1.Seat"

#define WAYDROID_DBUS_NAME          "id.waydro.Container"
#define WAYDROID_DBUS_PATH          "/ContainerManager"
#define WAYDROID_DBUS_INTERFACE     "id.waydro.ContainerManager"


struct _FreezerPrivate {
    GDBusProxy *waydroid_proxy;
    GDBusProxy *logind_proxy;
    guint freeze_timeout_id;
};


static gboolean
waydroid_session_started (Freezer *self) {
    GVariant *value;
    GVariantIter i;
    g_autoptr (GError) error = NULL;

    value = g_dbus_proxy_call_sync (
        self->priv->waydroid_proxy,
        "GetSession",
        NULL,
        G_DBUS_CALL_FLAGS_NONE,
        -1,
        NULL,
        &error
    );

    if (error != NULL) {
        g_warning ("Can't call GetSession: %s", error->message);
    } else {
        g_variant_iter_init (&i, value);
        return g_variant_iter_n_children (&i) != 0;
    }
    return FALSE;
}


static gboolean
waydroid_freeze (Freezer *self) {
    g_autoptr (GError) error = NULL;

    g_return_val_if_fail (waydroid_session_started (self), G_SOURCE_REMOVE);

    g_dbus_proxy_call_sync (
        self->priv->waydroid_proxy,
        "Freeze",
        NULL,
        G_DBUS_CALL_FLAGS_NONE,
        -1,
        NULL,
        &error
    );

    if (error != NULL) {
        g_warning("Can't call Freeze: %s", error->message);
    } else {
        g_debug ("Waydroid freeze");
    }

    return G_SOURCE_REMOVE;
}


static void
waydroid_hide_all_apps (Freezer *self) {
    gint exit_status = 0;

    g_return_if_fail (waydroid_session_started (self));

    g_spawn_command_line_sync(
        "waydroid shell -- am start -W -c android.intent.category.HOME -a android.intent.action.MAIN",
        NULL,
        NULL,
        &exit_status,
        NULL
    );

    if (exit_status != 0) {
        g_warning("Can't hide applications");
    }
}

static void
logind_proxy_properties_cb (GDBusProxy* proxy,
                            GVariant* changed_properties,
                            char** invalidated_properties,
                            gpointer user_data)
{
    Freezer *self = user_data;
    GVariant *value;
    GVariantIter i;
    gchar *property;
    gboolean locked = FALSE;

    g_debug ("logind properties changed");

    g_variant_iter_init (&i, changed_properties);
    while (g_variant_iter_next (&i, "{&sv}", &property, &value)) {
        if (g_strcmp0 (property, "IdleHint") == 0) {
            locked = g_variant_get_boolean (value);
        }
        g_variant_unref (value);
    }

    if (locked) {
        /*
         * We do not want to unfreeze on unlock (waydroid app launcher job)
         * but we also do not want frozen apps on unlock, hiding does the job
         */
        waydroid_hide_all_apps (self);
        self->priv->freeze_timeout_id =
            g_timeout_add (FREEZE_TIMEOUT, (GSourceFunc) waydroid_freeze, self);
    } else {
        g_clear_handle_id (&self->priv->freeze_timeout_id, g_source_remove);
    }
}


G_DEFINE_TYPE_WITH_CODE (
    Freezer,
    freezer,
    G_TYPE_OBJECT,
    G_ADD_PRIVATE (Freezer)
)


static void
freezer_dispose (GObject *freezer)
{
    Freezer *self = FREEZER (freezer);

    g_clear_object (&self->priv->logind_proxy);
    g_clear_object (&self->priv->waydroid_proxy);

    g_free (self->priv);

    G_OBJECT_CLASS (freezer_parent_class)->dispose (freezer);
}


static void
freezer_class_init (FreezerClass *klass)
{
    GObjectClass *object_class;

    object_class = G_OBJECT_CLASS (klass);
    object_class->dispose = freezer_dispose;
}


static void
freezer_init (Freezer *self)
{
    self->priv = freezer_get_instance_private (self);

    self->priv->waydroid_proxy =
                g_dbus_proxy_new_for_bus_sync (
                    G_BUS_TYPE_SYSTEM,
                    0,
                    NULL,
                    WAYDROID_DBUS_NAME,
                    WAYDROID_DBUS_PATH,
                    WAYDROID_DBUS_INTERFACE,
                    NULL,
                    NULL
                );

    if (self->priv->waydroid_proxy == NULL) {
            g_error ("No %s bus",  WAYDROID_DBUS_NAME);
    }

    self->priv->logind_proxy =
                g_dbus_proxy_new_for_bus_sync (
                    G_BUS_TYPE_SYSTEM,
                    0,
                    NULL,
                    LOGIND_DBUS_NAME,
                    LOGIND_DBUS_PATH,
                    LOGIND_DBUS_INTERFACE,
                    NULL,
                    NULL
               );

    if (self->priv->logind_proxy == NULL) {
            g_error ("No %s bus",  LOGIND_DBUS_NAME);
    }

    g_signal_connect (
        self->priv->logind_proxy,
        "g-properties-changed",
        G_CALLBACK (logind_proxy_properties_cb),
        self
    );
}


/**
 * freezer_new:
 *
 * Creates a new #Freezer
 *
 * Returns: (transfer full): a new #Freezer
 *
 **/
GObject *
freezer_new (void)
{
    GObject *freezer;

    freezer = g_object_new (TYPE_FREEZER, NULL);

    return freezer;
}
