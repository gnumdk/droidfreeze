# Droid Freeze daemon

This small service freezes Waydroid on screen lock (You've been warned, don't expect Android to run with this when screen is off)

Waydroid already provides an auto suspend option but it is based on PowerManagerService and sometimes, it just never sleeps.

So if you want to use this, to prevent dead locks, you first need to disable Waydroid auto sleep:
 
`$ waydroid prop set persist.waydroid.suspend false`


## Depends on

- `glib2`
- `meson`
- `ninja`

## Building from Git

```bash
$ meson builddir --prefix=/usr

$ sudo ninja -C builddir install
```
